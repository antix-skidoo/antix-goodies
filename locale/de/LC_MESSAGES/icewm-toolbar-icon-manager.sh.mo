��          �            x     y     �  0   �     �     �  d   �  D   +  H   p     �  e   �     +  "   J     m     r     z  o  �     �       A        \     d  �   j  m     h   {     �  m   �  '   c  +   �     �     �     �                                             
         	                         ADD icon ADVANCED Double click any Application to remove its icon. EXIT HELP No changes were made!\nPlease choose an application's .desktop file to add its icon the the toolbar. No changes were made!\nTIP: you can always try the Advanced buttton. Please select any option from the buttons below to manage Toolbar icons. REMOVE icon To add a new icon to the toolbar choose your applications's .desktop file then click the 'Ok' button. Toolbar Icon Manager for IceWM Toolbar Icon Manager for IceWM v.9 UNDO Warning file is empty Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-05-27 15:48+0300
Last-Translator: delix02, 2020
Language-Team: German (https://www.transifex.com/anticapitalista/teams/10162/de/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: de
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
 Symbol HINZUFÜGEN WEITERFÜHREND Zum Entfernen des Programmsymbols das Programm zweimal anklicken. BEENDEN HILFE Es wurden keine Änderungen vorgenommen!\n Um das Symbol eines Anwendungsprogramms Werkzeugleiste hinzuzufügen bitte die .desktop Datei des Programms auswählen. Es wurden keine Änderungen vorgenommen!\n HINWEIS: vielleicht hilft die WEITERFÜHREND Schaltfläche weiter. Zur Verwaltung der Symbole der Werkzegleiste bitte eine der Optionen mit den Schaltflächen auswählen.  Symbol ENTFERNEN Um eine neue Schaltfläche hinzuzufügen die .desktop Datei des Programms auswählen und dann 'OK' anklicken. Verwaltung der Werkzeugleiste bei IceWM Verwaltung der Werkzeugleiste bei IceWM v.9 RÜCKGÄNGIG MACHEN Warnung Datei ist leer 