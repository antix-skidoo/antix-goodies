��    	      d      �       �   [   �   B   =  0   �  �   �  G   ?  .   �  W   �  9     �  H    F  �   I  �     l  �  �   	  �   �	  �   v
  �   R                                          	    A removable drive with a mounted partition was not found.
It is safe to unplug the drive(s) About to unmount:
$summarylist
Please confirm you wish to proceed. Data is being written to devices.
Please wait... Mountpoint removal failed.

A mountpoint remains present at:
$mountpointerrorlist
Check each mountpoint listed before unpluging the drive(s). Nothing has been unmounted.
Aborting as requested with no action taken. Nothing selected.
Aborting without unmounting. The following are currently mounted:
$removablelist
Choose the drive(s) to be unplugged Unmounted:
$summarylist
It is safe to unplug the drive(s) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2011-12-15 00:36+0200
PO-Revision-Date: 2018-09-23 15:53+0300
Last-Translator: Panwar108 <caspian7pena@gmail.com>
Language-Team: Hindi (http://www.transifex.com/anticapitalista/antix-development/language/hi/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: hi
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.8.11
X-Launchpad-Export-Date: 2012-01-02 10:46+0000
 माउंट हो रखे विभाजन युक्त कोई हटाने योग्य ड्राइव नहीं मिली।
ड्राइव(एक या एकाधिक) हटाना सुरक्षित है इन्हें माउंट से हटाया जाएगा :
$summarylist
पुष्टि करें कि आप जारी रखना चाहते हैं। डिवाइस पर डाटा राइट किया जा रहा है।
कृपया, प्रतीक्षा करें... माउंट पॉइंट हटाना विफल रहा।

यहाँ अब भी एक माउंट पॉइंट मौजूद है :
$mountpointerrorlist
ड्राइव(एक या एकाधिक) हटाने से पहले हर प्रदर्शित माउंट पॉइंट जाँचें। माउंट से कुछ नहीं हटाया गया।
अनुरोध अनुसार बिना कुछ करें ही बंद किया जा रहा है। कुछ चयनित नहीं है।
माउंट से हटाए बिना ही बंद किया जा रहा है। निम्नलिखित अभी माउंट हो रखें हैं :
$removablelist
हटाए जाने वाली ड्राइव (एक या एकाधिक) को चुनें माउंट से हटाए गए :
$summarylist
ड्राइव(एक या एकाधिक) हटाना सुरक्षित है 