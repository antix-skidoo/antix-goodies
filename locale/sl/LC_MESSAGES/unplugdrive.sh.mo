��          \      �       �   [   �   B   %  �   h  G   �  .   >  W   m  9   �  8  �  [   8  F   �  �   �  P   �  -   �  `     ?   l                                       A removable drive with a mounted partition was not found.
It is safe to unplug the drive(s) About to unmount:
$summarylist
Please confirm you wish to proceed. Mountpoint removal failed.

A mountpoint remains present at:
$mountpointerrorlist
Check each mountpoint listed before unpluging the drive(s). Nothing has been unmounted.
Aborting as requested with no action taken. Nothing selected.
Aborting without unmounting. The following are currently mounted:
$removablelist
Choose the drive(s) to be unplugged Unmounted:
$summarylist
It is safe to unplug the drive(s) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2011-12-15 00:36+0200
PO-Revision-Date: 2019-10-13 17:45+0300
Last-Translator: Arnold Marko <arnold.marko@gmail.com>
Language-Team: Slovenian (http://www.transifex.com/anticapitalista/antix-development/language/sl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sl
Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3);
X-Generator: Poedit 1.8.11
X-Launchpad-Export-Date: 2012-01-02 10:46+0000
 Izmenljiv pogon s priklopljenim razdelkom ni bil najden.
Zdaj je varno odstraniti pogon(e). Pripravljeni za odklop:
$summarylist
Prosimo potrdite za nadaljevanje. Odstranitev priklopne točke ni bila uspešna.

Točka priklopa ostaja prisotna na:
$mountpointerrorlist
Prverite vsako priklopno točko iz seznama, preden odstranite pogon(e). Nič ni bilo odklopljeno.
Prekinjam, kot je bilo zahtevano, ne da bi kaj storil. Nič ni izbrano.
Prekinjam brez odklapljanja. Naslednji so trenutni prikloljeni:
$removablelist
Izberite pogon(e), ki naj bo(do) odklopljen(i) Odklopljeno:
$summarylist
Sedaj lahko varno odstranite pogon(e) 