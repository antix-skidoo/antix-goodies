��          �      �       0     1     B  �   T  G  �  B     $   a  3   �     �     �  F   �     $     5  �  =     	     "  �   ?  �  �  O   T
  *   �
  &   �
     �
       F        d  	                                   
                      	    ADD selected app App .desktop file Choose (or drag and drop to the field below) the .desktop file you want to add to the personal menu \n OR select any other option FPM has no 'graphical' way to allow users to move icons around or delete arbitrary icons.\nIf you click OK, the personal menu configuration file will be opened for editing.\nEach menu icon is identified by a line starting with 'prog' followed by the application name, icon location and the application executable file.\nMove or delete the entire line refering to each personal menu entry.\nNote: Lines starting with # are comments only and will be ignored.\nThere can be empty lines.\nSave any changes and then restart IceWM.\nYou can undo the last change from FPMs 'Restore' button. FTM is programmed to always keep 1 line in the personal menu file! Fast Personal Menu Manager for IceWM No changes were made! Please choose an application. ORGANIZE entries REMOVE last entry This will delete the last entry from your personal menu! Are you sure? UNDO last change Warning Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-03-26 19:09+0200
Last-Translator: anticapitalista <anticapitalista@riseup.net>, 2020
Language-Team: Slovenian (https://www.transifex.com/anticapitalista/teams/10162/sl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sl
Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3);
X-Generator: Poedit 2.3
 DODAJ izbrane aplikacije Datoteka .desktop aplikacije Izberite (ali povlecite in spustite v spodnjo polje) .deskto datoteko, ki bi jo radi dodli v osebni meni\nALI izberite drugo opcijo. FPM nima 'grafičnega' načina, ki bi uporabniku omogočal premikanje ikon ali brsianje poljubnih ikon.\nČe kliknete na V redu, se bo za urejanje odprla konfiguracijska datoteka osebnega menija.\nVsaka ikona menija je označena z vrstico, ki se začne s 'prog', tej sledi ime aplikacije, lokacija ikone in izvršijive datoteke.\nPremaknite ali izbrišite celotno vrstico, ki se nanaša na vnos v osebnem meniju.\nOpomba: Vrstie, ki se začnejo z znakom # so kompentarji in ne bodo upoštevane.\nLahko obstajajo prazne vrstice.\nShranite spremembe in nato ponovno zaženite IceWM.\nZadnjo spremembo je mogoče razveljaviti s pomočjo gumba 'Obnovi' FPM-a. FTM je programiran tako, da v datoteki osebnega menija vedno obdrži 1 vrstico. Hitro upravljanje osebnih menijev za IceWM Ni bilo sprememb! Izberite aplikacijo. ORGANIZIRAJ vnose ODSTRANI zadnji vnos To bo iz osebnega imenika odstranilo zadnji vnos! Ali ste prepričani? RAZVEJAVI zadnjo spremembo Opozorilo 