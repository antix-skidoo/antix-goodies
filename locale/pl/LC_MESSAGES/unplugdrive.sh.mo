��    	      d      �       �   [   �   B   =  0   �  �   �  G   ?  .   �  W   �  9     �  H  e   �  O   <  8   �  �   �  U   x  9   �  I     C   R                                          	    A removable drive with a mounted partition was not found.
It is safe to unplug the drive(s) About to unmount:
$summarylist
Please confirm you wish to proceed. Data is being written to devices.
Please wait... Mountpoint removal failed.

A mountpoint remains present at:
$mountpointerrorlist
Check each mountpoint listed before unpluging the drive(s). Nothing has been unmounted.
Aborting as requested with no action taken. Nothing selected.
Aborting without unmounting. The following are currently mounted:
$removablelist
Choose the drive(s) to be unplugged Unmounted:
$summarylist
It is safe to unplug the drive(s) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2011-12-15 00:36+0200
PO-Revision-Date: 2019-10-13 17:46+0300
Last-Translator: Filip Bog <mxlinuxpl@gmail.com>
Language-Team: Polish (http://www.transifex.com/anticapitalista/antix-development/language/pl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pl
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : (n%10>=2 && n%10<=4) && (n%100<12 || n%100>14) ? 1 : n!=1 && (n%10>=0 && n%10<=1) || (n%10>=5 && n%10<=9) || (n%100>=12 && n%100<=14) ? 2 : 3);
X-Generator: Poedit 1.8.11
X-Launchpad-Export-Date: 2012-01-02 10:46+0000
 Nie znaleziono dysku przenośnego z zamontowaną partycją.
Można bezpiecznie odłączyć napęd(y). Przygotowane do odmontowania:
$summarylist
Potwierdź, że chcesz kontynuować. Dane są zapisywane na urządzeniach.
Proszę czekać... Usuwanie punktu montowania nie powiodło się.

Punkt montowania pozostaje obecny w:
$mountpointerrorlist
Sprawdź każdy wymieniony punkt montowania zanim odłączysz napęd(y). Nic nie zostało odmontowane.
Przerwanie zgodnie z żądaniem bez żadnych działań. Nic nie zostało zaznaczone.
Przerwanie bez odmontowania. Obecnie są zamontowane:
$removablelist
Wybierz napęd(y) do odłączenia Odmontowano:
$summarylist
Można bezpiecznie odłączyć napęd(y). 