#!/bin/bash
#Date and Time Setting Tool Copyright 2009,2011 by Tony Brijeski under the GPL V2
# modified by skidoo and ppc - https://pastebin.com/1YmJHb95
###   NOTE: no validation is performed ~~ user can choose "Feb 31"
 
DIALOG="`which yad` --width 400 --center --undecorated"
TITLE="--always-print-result --dialog-sep --title="
TEXT="--text="
ENTRY="--entry "
ENTRYTEXT="--entry-text "
MENU="--list --print-column=1 --column=Pick:HD --column=_"
YESNO="--question "
MSGBOX="--info "
SCALE="--scale "
PASSWORD="--entry --hide-text "
TITLETEXT="Manage Date and Time Settings"
testroot="`whoami`"   #  howdy       backticks galore
 
if [ "$testroot" != "root" ]; then
    gksu $0
    exit 1
fi
 
while [ "$SETCHOICE" != "Exit" ]; do
DAY="`date +%d`"
MONTH="`date +%m`"
YEAR="`date +%Y`"
MINUTE="`date +%M`"
HOUR="`date +%H`"
SETCHOICE=`$DIALOG --no-buttons --center --height 300 $TITLE"$TITLETEXT" $MENU $TEXT" Manage Date and Time Settings\n Time Zone: $(cat /etc/timezone) \n Time: $HOUR:$MINUTE\n Date: $MONTH-$DAY-$YEAR\n\n" SETTIME " Set Current Time" SETDATE " Set Current Date"  SETTZ " Choose Time Zone (using cursor and enter keys)"  SETAUTO " Use Internet Time server to set automaticaly time/date" Exit " Quit"`
SETCHOICE=`echo $SETCHOICE | cut -d "|" -f 1`
 
if [ "$SETCHOICE" = "SETTIME" ]; then
    HOUR="`date +%H`"
    HOUR=`echo $HOUR | sed -e 's/^0//g'`
    SETHOUR=`$DIALOG --center $TITLE"$TITLETEXT" $SCALE --value=$HOUR --min-value=0 --max-value=23 $TEXT"Move the slider to the correct Hour"`
    if [ "$?" = "0" ]; then
        if [ "${#SETHOUR}" = "1" ]; then
            SETHOUR="0$SETHOUR"
        fi
 
        MINUTE="`date +%M`"
        MINUTE=`echo $MINUTE | sed -e 's/^0//g'`
    fi
 
    SETMINUTE=`$DIALOG --center $TITLE"$TITLETEXT" $SCALE --value=$MINUTE --min-value=0 --max-value=59 $TEXT"Move the slider to the correct Minute"`
    if [ "$?" = "0" ]; then
        if [ "${#SETMINUTE}" = "1" ]; then
            SETMINUTE="0$SETMINUTE"
        fi
 
        date $MONTH$DAY$SETHOUR$SETMINUTE$YEAR
        hwclock --systohc
    fi
fi
 
if [ "$SETCHOICE" = "SETDATE" ]; then
    var=`$DIALOG --form --separator="" --date-format="%Y%m%d" --field="Date:":DT`
SETYEAR=$(echo ${var:0:4})
SETMONTH=$(echo ${var:4:2})
SETDAY=$(echo ${var:6:2})
MINUTE="`date +%M`"
HOUR="`date +%H`"
sudo date $SETMONTH$SETDAY$HOUR$MINUTE$SETYEAR
                hwclock --systohc
fi
 
if [ "$SETCHOICE" = "SETAUTO" ]; then
sudo date -s "$(wget -qSO- --max-redirect=0 google.com 2>&1 | grep Date: | cut -d' ' -f5-8)Z"
                hwclock --systohc
fi
 
if [ "$SETCHOICE" = "SETTZ" ]; then
sudo roxterm --hide-menubar -z 0.75 -T " Select Time Zone" -e /bin/bash -c "dpkg-reconfigure tzdata"
 
fi
done
 
exit 0