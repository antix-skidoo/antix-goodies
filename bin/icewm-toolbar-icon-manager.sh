#!/bin/bash
# Adds/Removes icons to the IceWM desktop toolbar- antiX 19(adding icons is done using the info from the app's .desktop file) 
# By PPC, 27/2/2020 adapted from many, many on-line examples
# No licence what so ever- feel free to improve/adapt this script
# To do: 1- allow to move icons ### 
# localisation added by anticapitalista - 11-03-2020

TEXTDOMAINDIR=/usr/share/locale
TEXTDOMAIN=icewm-toolbar-icon-manager.sh

help()
{
		###Function to display help
		yad --window-icon=/usr/share/pixmaps/icewm_editor.png --center --form --title=$"Toolbar Icon Manager for IceWM" --field="Help::TXT" $"What is this?\nThis utility adds and removes application icons to IceWm's toolbar.\nThe toolbar application icons are created from an application's .desktop file.\nWhat are .desktop files?\nUsually a .desktop file is created during an application's installation process to allow the system easy access to relevant information, such as the app's full name, commands to be executed, icon to be used, where it should be placed in the OS menu, etc.\nA .desktop file name usually refers to the app's name, which makes it very easy to find the intended .desktop file (ex: Firefox ESR's .desktop file is 'firefox-esr.desktop').\nWhen adding a new icon to the toolbar, the user can click the field presented in the main window and a list of all the .desktop files of the installed applications will be shown.\nThat, in fact, is a list of (almost) all installed applications that can be added to the toolbar.\n Note: some of antiX's applications are found in the sub-folder 'antiX'.\n
TIM buttons:\n'ADD icon' - show a list of .desktop files of installed applications. Double click the application you want and its icon instantly shows up on the toolbar.\nIf, for some reason, TIM fails to find the correct icon for your application, it will still create a toolbar icon using the default 'gears' image so that you can still click to access the application.\nYou can click the 'Advanced' button to manually edit the relevant entry and change the application's icon.\n'UNDO' - every time an icon is added or removed from the toolbar, TIM creates a backup file. If you click this button, the toolbar is instantly restored from that backup file without any confirmation.\n'REMOVE icon' - this shows a list of all applications that have icons on the toolbar. Double left click any application to remove its icon from the toolbar.\n'ADVANCED' - allows for editing the text configuration file that has all of your desktop's toolbar icon's configurations. Manually editing this file allows the user to rearange the order of the icons and delete or add any icon. A brief explanation about the inner workings of the text configuration file is displayed before the file is opened for editing.\nWarnings: only manually edit a configuration file if you are sure of what you are doing! Always make a back up copy before editing a configuration file! " --center --width=600 --height=700 --button=gtk-quit:1
		###END of Function to display help
}

advanced()
{
		###Function to manually manage icons (ADVANCED management)
		cp ~/.icewm/toolbar ~/.icewm/toolbar.bak &&	
		yad --window-icon=/usr/share/pixmaps/icewm_editor.png --center --form --title=$"Toolbar Icon Manager for IceWM" --field="Warning::TXT" $"Note: TIM has no 'graphical' way to allow users to move icons around.\nIf you click OK, the toolbar configuration file will be opened for editing.\n
How-to:\nEach toolbar icon is identified by a line starting with 'prog' followed by the application name, icon and the application executable file.\nMove, edit or delete any line you want.\nNote: Lines starting with # are comments only and will be ignored.\nThere can be empty lines.\nSave any changes and then restart iceWM.\nYou can undo the last change from TIM's Undo button." --width=500 --height=360 --button=gtk-no:1 --button=gtk-yes:0 && geany ~/.icewm/toolbar
		###END of Function to manually arrange icons
}		

delete_icon()
{
		###Function to delete  icon
		#create backup file before changes
		cp ~/.icewm/toolbar ~/.icewm/toolbar.bak
		### Select any application whose icon you want to remove from the toolbar:
		#display only application names
		sed '/.*\"\(.*\)\".*/ s//\1/g' ~/.icewm/toolbar > /tmp/toolbar-test-edit0.txt
		# do not show commented lines
		egrep -v '^(;|#|//)' /tmp/toolbar-test-edit0.txt > /tmp/toolbar-test-edit.txt
		#choose application to delete
		EXEC=$(yad --window-icon=/usr/share/pixmaps/icewm_editor.png --title=$"Toolbar Icon Manager for IceWM" --width=450 --height=480 --center --separator=" " --list  --column=$"Double click any Application to remove its icon."  < /tmp/toolbar-test-edit.txt --button=$'Remove':4)
		#get line number(s) where the choosen application is
		x=$(echo $EXEC)
		 Line=$(sed -n "/$x/=" ~/.icewm/toolbar)
		 ## NOTE: to be on the safe side, in order to use $Line to delete the line(s) that match the selection, first extract it's first number, to avoid errors in case more than one line matchs the selection (there's more that one icon for the same app on the toolbar), TIM should only delete the first occorrence!!! Also: changed the sed command so it directly deletes line number $Line (solves the bug of not deleting paterns with spaces)...
		 firstx=$(echo $Line | grep -o -E '[0-9]+' | head -1 | sed -e 's/^0\+//')
		# remove the first line that matchs the user selection and save that into a temporary file
		sed ${firstx}d ~/.icewm/toolbar > ~/.tempo
        # copy that temp file to antiX's icewm toolbar file, delete the temp file and restart to see changes BUT only if "toolbar" file is not rendered completly empty after changes (fail safe to avoid deleting the entire toolbar icon's content, in case a icon has a description with \|/*, etc.)
         if [[ -s ~/.tempo ]];
     then echo "file has something";
     #file is not empty
					cp ~/.tempo ~/.icewm/toolbar ;
					del ~/.tempo ;
					icewm --restart ;
					exit
      else echo $"file is empty";
      #file is empty
		yad --window-icon=/usr/share/pixmaps/icewm_editor.png --title=$"Warning" --text=$"No changes were made!\nTIP: you can always try the Advanced buttton." --timeout=3 --no-buttons --center
		 fi
        	exit
		###END of Function to delete last icon
}		

restore_icon()
{
		###Function to restore last backup
cp ~/.icewm/toolbar.bak ~/.icewm/toolbar
icewm --restart
		###END Function to restore last backup
}

add_icon()
{
	DADOS=$(yad --window-icon=/usr/share/pixmaps/icewm_editor.png --length=800 --width=800 --center --file --filename=/usr/share/applications
 --title=$"Toolbar Icon Manager for IceWM v.9" \
--wrap --text=$"To add a new icon to the toolbar choose your applications's .desktop file then click the 'Ok' button.")
		###Function to add a new icon
COMANDO0=$(echo "$DADOS" | cut -d'|' -f1)
#this strips any existing path from the name:
EXEC0=$(grep Exec= $COMANDO0 | cut -d '=' -f2)
EXEC=$(echo "$EXEC0" | cut -f1 -d" ")
#this strips any existing path from the name:
COMANDO00=$(basename $COMANDO0)
#this strips any existing .desktop from the name:
REMOVE=".desktop"
##edit here to use real application name:
#NOME=${COMANDO00//$REMOVE/}
app_path='/usr/share/applications/'
full_path=$(echo $app_path$EXEC)
NOME=$(grep '^Name=' $COMANDO0 | head -1 | tail -1 | sed 's/^Name=//' )

 #try to find app icon:
	ICON0=$(grep Icon= $COMANDO0 | cut -d '=' -f2)
	ICON00=$(echo "$ICON0" | cut -f1 -d" ")
	ICONwithoutpath=$(basename $ICON00)

# By default set the icon as the gears icon, then look if the icon exist in several paths...
ICONE="/usr/share/icons/papirus-antix/24x24/apps/yast-runlevel.png"

# if a icon with a full path exists on the .desktop, use that icon
if [[ -f "$ICON00" ]]; then  ICONE=$ICON00
fi

#...Also check if the icon's name exists in several possible default paths, if a existing icon is found, use that instead!
#We can add as many paths as we want for the system to look for icons, also, we can look for icons with extensions other than .png (ex: svg), adding new "extension" and path's, and repeating the if-fi cicle
extension=".png"

path="/usr/share/pixmaps/"
if [[ -f "$path$ICONwithoutpath$extension" ]]; then  ICONE=$path$ICONwithoutpath$extension
fi

path="/usr/share/icons/papirus-antix/24x24/apps/"
if [[ -f "$path$ICONwithoutpath$extension" ]]; then  ICONE=$path$ICONwithoutpath$extension
fi

path="/usr/share/icons/papirus-antix/24x24/places/"
if [[ -f "$path$ICONwithoutpath$extension" ]]; then  ICONE=$path$ICONwithoutpath$extension
fi

## v.9 - looks in another folder, that has icon's for example, for Brave Browser
path="/usr/share/icons/hicolor/24x24/apps/"
if [[ -f "$path$ICONwithoutpath$extension" ]]; then  ICONE=$path$ICONwithoutpath$extension
fi
   
## v.9 – if no icon was found after searching the default couple of icon folders, perform active search for icons and use search result only if an icon was found- it takes about 1 second, but almost always finds a icon!
default="/usr/share/icons/papirus-antix/24×24/apps/yast-runlevel.png"
if [ "$ICONE" == "$default" ]; then
  search=$(locate /usr/share/icons/*/$ICONwithoutpath$extension)
  first_result=$(echo $search | head -n1 | awk '{print $1;}')
  if [ -z "$first_result" ]
  then 
  echo "No icon located, using default Gears icon"
  else 
  echo "Icon located!" ; ICONE=${first_result::-4}
  fi
fi
###
 
# error if no application selected- avoids creating empty icon on toolbar:
if [ -z "$EXEC" ]; then yad --window-icon=/usr/share/pixmaps/icewm_editor.png --title=$"Warning" --text=$"No changes were made!\nPlease choose an application's .desktop file to add its icon the the toolbar." --timeout=3 --no-buttons --center
	exit
fi

#create backup file before changes
cp ~/.icewm/toolbar ~/.icewm/toolbar.bak

#open .desktop file and get EXEC= contents
EXEC0=$(grep Exec= $COMANDO0 | cut -d '=' -f2)
#in case EXEC has more than one line, use only the first
readarray -t lines < <(echo "$EXEC0")
EXECperc="${lines[0]}"

#add line to toolbar - the | cut -f1 -d"%"   part removes any %x option from the exec command.
echo "prog "\"${NOME}"\" "${ICONE}" "${EXECperc}""| cut -f1 -d"%"  >> ~/.icewm/toolbar
#instantly restart IceWm so the new icon appears
icewm --restart
		###END of Function to add a new icon
}

export -f help delete_icon advanced restore_icon add_icon

DADOS=$(yad --window-icon=/usr/share/pixmaps/icewm_editor.png --length=800 --width=800 --center --paned --splitter="200" --title="Toolbar Icon Manager for IceWM v.9" \
--form  \
--button=$"EXIT":1 \
--button=$"HELP":"bash -c help" \
--button=$"ADVANCED":"bash -c advanced" \
--button=$"REMOVE icon":"bash -c delete_icon" \
--button=$"UNDO":"bash -c restore_icon" \
--button=$"ADD icon":"bash -c add_icon" \
--wrap --text=$"Please select any option from the buttons below to manage Toolbar icons.")

### wait for a button to be pressed then perform the selected funtion
foo=$?

[[ $foo -eq 1 ]] && exit 0